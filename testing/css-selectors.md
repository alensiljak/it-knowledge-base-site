# CSS Selectors

Knowing CSS Selectors in detail can be very helpful for UI or End-to-End testing, as it provides a convenient way locate elements for testing.

In general, XPath and CSS have a very similar performance.

## Reference

- [CSS Selectors](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors), Mozilla Developer Network
