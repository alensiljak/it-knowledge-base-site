# Testing

This is a broad topic.

Generally, testing may be divided based on the scope of tests:

- Unit Testing
- Integration Testing 
- UI Testing
- End-to-end testing

It can also be viewed thematically:

- [API Testing](API-testing)
- [Security Testing](security-testing)
- User (Acceptance) Testing
- Quality Assurance Testing

## Useful Tools

- [CSS Selectors](css-selectors)
