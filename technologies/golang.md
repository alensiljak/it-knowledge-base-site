# Golang

Go or, better known as, Golang.

## Books

- https://www.gitbook.com/book/astaxie/build-web-application-with-golang

## Android

Golang apps can run on mobile operating systems Android and iOS.

Compilation for Android requires the presence of the Android NDK on the build machine.

The support is provided by the `gomobile` package.

- [gomobile](https://godoc.org/golang.org/x/mobile/cmd/gomobile)
- [Mobile Wiki](https://github.com/golang/go/wiki/Mobile)
- [Example apps](https://godoc.org/golang.org/x/mobile/example)
- [Go Arm](https://github.com/golang/go/wiki/GoArm)

## Database Access

- https://astaxie.gitbooks.io/build-web-application-with-golang/en/05.3.html
- [driver](https://github.com/mattn/go-sqlite3)

## REST API

- Tutorial [Creating a RESTful API With Golang](https://tutorialedge.net/golang/creating-restful-api-with-golang/)
