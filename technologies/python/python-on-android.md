# Python on Android

Python on Android page ([link](https://wiki.python.org/moin/Android)) shows several options are available:

- [Kivy](https://sites.google.com/site/alensit/technologies/python/kivy)
  - python for android ([link](https://github.com/kivy/python-for-android))
    - Android apps with Python, Flask and a WebView ([link](http://inclem.net/2016/05/07/kivy/python_for_android_webview_support/))
- https://github.com/kuri65536/python-for-android
- [BeeWare](https://beeware.org/)
  - [Toga](https://pybee.org/project/projects/libraries/toga/) is a cross-platform widget toolkit. [docs](https://toga.readthedocs.io/en/latest/)
  - Briefcase packages the app for different platforms. [docs](https://briefcase.readthedocs.io/en/latest/)
  - [VOC](https://pybee.org/project/projects/bridges/voc) is a transpiler that translates Python into Java bytecode for Android apps.

Č